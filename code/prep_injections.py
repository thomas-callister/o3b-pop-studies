import numpy as np
import h5py
import sys
import pickle
from support import *
import astropy.cosmology as cosmo
import astropy.units as u
from astropy.cosmology import Planck15

import sys
sys.path.append('/home/thomas.callister/CBC/effective-spin-priors/')
from priors import chi_effective_prior_from_isotropic_spins
from priors import joint_prior_from_isotropic_spins

def loadO3aInjections(ifar_threshold):

    # Load O1/O2/O3 injections
    # See the following pages for documentation
    # https://git.ligo.org/reed.essick/o1-o2-semianalytic-injections/-/wikis/O1+O2-Semianalytic-Injections/Overview
    # https://git.ligo.org/RatesAndPopulations/lvc-rates-and-pop/-/tree/master/share/O3
    #mockDetections_O3 = h5py.File('/home/reed.essick/rates+pop/o3-sensitivity-estimates/LIGO-T2100113-v10/endo3_bbhpop-LIGO-T2100113-v10-1238166018-15843600.hdf5','r')
    mockDetections_O3 = h5py.File('/home/reed.essick/rates+pop/o3-sensitivity-estimates/LIGO-T2100113-v10/endo3_bbhpop-LIGO-T2100113-v10.hdf5','r')

    # Read out total number of trials, downsampled to our target rate
    nTrials = mockDetections_O3.attrs['total_generated']

    # O3!
    ifar_1 = mockDetections_O3['injections']['ifar_gstlal'].value
    ifar_2 = mockDetections_O3['injections']['ifar_pycbc_bbh'].value
    ifar_3 = mockDetections_O3['injections']['ifar_pycbc_hyperbank'].value
    detected_O3 = np.where((ifar_1>ifar_threshold) | (ifar_2>ifar_threshold) | (ifar_3>ifar_threshold))[0]
    m1_detected_O3 = mockDetections_O3['injections']['mass1_source'].value[detected_O3]
    m2_detected_O3 = mockDetections_O3['injections']['mass2_source'].value[detected_O3]
    s1x_detected_O3 = mockDetections_O3['injections']['spin1x'].value[detected_O3]
    s1y_detected_O3 = mockDetections_O3['injections']['spin1y'].value[detected_O3]
    s1z_detected_O3 = mockDetections_O3['injections']['spin1z'].value[detected_O3]
    s2x_detected_O3 = mockDetections_O3['injections']['spin2x'].value[detected_O3]
    s2y_detected_O3 = mockDetections_O3['injections']['spin2y'].value[detected_O3]
    s2z_detected_O3 = mockDetections_O3['injections']['spin2z'].value[detected_O3]
    z_detected_O3 = mockDetections_O3['injections']['redshift'].value[detected_O3]
    precomputed_p_m1m2_O3 = mockDetections_O3['injections']['mass1_source_mass2_source_sampling_pdf'].value[detected_O3]
    precomputed_p_z_O3 = mockDetections_O3['injections']['redshift_sampling_pdf'].value[detected_O3]
    precomputed_p_m1m2z_O3 = precomputed_p_m1m2_O3*precomputed_p_z_O3
    run_O3 = np.full(m1_detected_O3.size,'O3')

    return m1_detected_O3,m2_detected_O3,s1x_detected_O3,s1y_detected_O3,s1z_detected_O3,s2x_detected_O3,s2y_detected_O3,s2z_detected_O3,z_detected_O3,precomputed_p_m1m2z_O3,run_O3,nTrials

def loadInjectionsAlt(ifar_threshold,snr_threshold):

    mockDetections = h5py.File('/home/reed.essick/rates+pop/o1+o2+o3-sensitivity-estimates/LIGO-T2100377-v2/o1+o2+o3_bbhpop_real+semianalytic-LIGO-T2100377-v2.hdf5','r')

    ifar_1 = mockDetections['injections']['ifar_gstlal'][()]
    ifar_2 = mockDetections['injections']['ifar_pycbc_bbh'][()]
    ifar_3 = mockDetections['injections']['ifar_pycbc_hyperbank'][()]
    snr = mockDetections['injections']['optimal_snr_net'][()]
    nTrials = mockDetections.attrs['total_generated']

    detected_O3 = np.where((ifar_1>ifar_threshold) | (ifar_2>ifar_threshold) | (ifar_3>ifar_threshold))[0]
    detected_O1O2 = np.where((mockDetections['injections']['name'][()]!=b'o3') & (snr>snr_threshold))[0]
    detected_full = np.concatenate([detected_O1O2,detected_O3])
    print(detected_O3.size,detected_O1O2.size,detected_full.size)

    m1_det = np.array(mockDetections['injections']['mass1_source'][()])[detected_full]
    m2_det = np.array(mockDetections['injections']['mass2_source'][()])[detected_full]
    s1x_det = np.array(mockDetections['injections']['spin1x'][()])[detected_full]
    s1y_det = np.array(mockDetections['injections']['spin1y'][()])[detected_full]
    s1z_det = np.array(mockDetections['injections']['spin1z'][()])[detected_full]
    s2x_det = np.array(mockDetections['injections']['spin2x'][()])[detected_full]
    s2y_det = np.array(mockDetections['injections']['spin2y'][()])[detected_full]
    s2z_det = np.array(mockDetections['injections']['spin2z'][()])[detected_full]
    z_det = np.array(mockDetections['injections']['redshift'][()])[detected_full]

    precomputed_p_m1m2z_spin = np.array(mockDetections['injections']['sampling_pdf'][()])[detected_full]
    p_inj_spin1 = (1./(4.*np.pi))*(1./0.998)/(s1x_det**2+s1y_det**2+s1z_det**2)
    p_inj_spin2 = (1./(4.*np.pi))*(1./0.998)/(s2x_det**2+s2y_det**2+s2z_det**2)
    precomputed_p_m1m2z = precomputed_p_m1m2z_spin/p_inj_spin1/p_inj_spin2

    runs = np.ones(m1_det.size)

    return m1_det,m2_det,s1x_det,s1y_det,s1z_det,s2x_det,s2y_det,s2z_det,z_det,precomputed_p_m1m2z,runs,nTrials

def loadInjections(ifar_threshold,snr_threshold):

    # Load O1/O2/O3 injections
    # See the following pages for documentation
    # https://git.ligo.org/reed.essick/o1-o2-semianalytic-injections/-/wikis/O1+O2-Semianalytic-Injections/Overview
    # https://git.ligo.org/RatesAndPopulations/lvc-rates-and-pop/-/tree/master/share/O3
    mockDetections_O1 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o1-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
    mockDetections_O2 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o2-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
    mockDetections_O3 = h5py.File('/home/reed.essick/rates+pop/o3-sensitivity-estimates/LIGO-T2100113-v10/endo3_bbhpop-LIGO-T2100113-v10.hdf5','r')

    # Compute rate at which injections were performed
    injectionRate_O1 = mockDetections_O1.attrs['total_generated']/mockDetections_O1.attrs['total_analysis_time']
    injectionRate_O2 = mockDetections_O2.attrs['total_generated']/mockDetections_O2.attrs['total_analysis_time']
    injectionRate_O3 = mockDetections_O3.attrs['total_generated']/mockDetections_O3.attrs['analysis_time_s']
    targetRate = min(injectionRate_O1,injectionRate_O2,injectionRate_O3)

    # O1 and O2 found injections do not take into account the imperfect duty cycle of detectors during these runs
    # In addition to dealing with differing sampling rates, we will need to further downselect O1 and O2 events
    # according to the estimated duty cycles in these runs
    # See the following for more information:
    # https://git.ligo.org/reed.essick/o1-o2-semianalytic-and-o3-real-injections
    # https://git.ligo.org/reed.essick/o1-o2-semianalytic-and-o3-real-injections/-/blob/main/make-mixtures
    obsTime_O1 = 4078080.
    obsTime_O2 = 10195200.
    dutyCycle_O1 = obsTime_O1/mockDetections_O1.attrs['total_analysis_time']
    dutyCycle_O2 = obsTime_O2/mockDetections_O2.attrs['total_analysis_time']

    # Read out total number of trials, downsampled to our target rate
    nTrials_O1 = mockDetections_O1.attrs['total_generated']*targetRate/injectionRate_O1
    nTrials_O2 = mockDetections_O2.attrs['total_generated']*targetRate/injectionRate_O2
    nTrials_O3 = mockDetections_O3.attrs['total_generated']*targetRate/injectionRate_O3
    nTrials = nTrials_O1 + nTrials_O2 + nTrials_O3

    # Read out network SNRs and select detected events from O1
    snr_network_O1 = mockDetections_O1['events']['snr_net']
    detected_O1 = np.where(snr_network_O1>snr_threshold)[0]

    # Now, we want to downsample so that we have a set of trials consistent with the target rate identified above
    # Achieve this by randomly drawing some reduced number of detections, according to the ratio between our target injection rate
    # and the actual O1 injection rate, multiplied by the fraction of time detectors were in locked observing mode
    detected_and_downsampled_O1 = np.random.choice(detected_O1,size=int(round(detected_O1.size*dutyCycle_O1*targetRate/injectionRate_O1)),replace=False)
    print("!!!!",len(detected_and_downsampled_O1))

    # Read out found injections and precomputed draw probabilities
    m1_detected_O1 = mockDetections_O1['events']['mass1_source'][detected_and_downsampled_O1]
    m2_detected_O1 = mockDetections_O1['events']['mass2_source'][detected_and_downsampled_O1]
    s1x_detected_O1 = mockDetections_O1['events']['spin1x'][detected_and_downsampled_O1]
    s1y_detected_O1 = mockDetections_O1['events']['spin1y'][detected_and_downsampled_O1]
    s1z_detected_O1 = mockDetections_O1['events']['spin1z'][detected_and_downsampled_O1]
    s2x_detected_O1 = mockDetections_O1['events']['spin2x'][detected_and_downsampled_O1]
    s2y_detected_O1 = mockDetections_O1['events']['spin2y'][detected_and_downsampled_O1]
    s2z_detected_O1 = mockDetections_O1['events']['spin2z'][detected_and_downsampled_O1]
    z_detected_O1 = mockDetections_O1['events']['z'][detected_and_downsampled_O1]
    precomputed_lnp_z_O1 = mockDetections_O1['events']['logpdraw_z'][detected_and_downsampled_O1]
    precomputed_lnp_m1_O1 = mockDetections_O1['events']['logpdraw_mass1_source_GIVEN_z'][detected_and_downsampled_O1]
    precomputed_lnp_m2_O1 = mockDetections_O1['events']['logpdraw_mass2_source_GIVEN_mass1_source'][detected_and_downsampled_O1]
    precomputed_p_m1m2z_O1 = np.exp(precomputed_lnp_z_O1+precomputed_lnp_m1_O1+precomputed_lnp_m2_O1)
    run_O1 = np.full(m1_detected_O1.size,'O1')

    # Do the same thing for O2!
    snr_network_O2 = mockDetections_O2['events']['snr_net']
    detected_O2 = np.where(snr_network_O2>snr_threshold)[0]
    detected_and_downsampled_O2 = np.random.choice(detected_O2,size=int(round(detected_O2.size*dutyCycle_O2*targetRate/injectionRate_O2)),replace=False)
    m1_detected_O2 = mockDetections_O2['events']['mass1_source'][detected_and_downsampled_O2]
    m2_detected_O2 = mockDetections_O2['events']['mass2_source'][detected_and_downsampled_O2]
    s1x_detected_O2 = mockDetections_O2['events']['spin1x'][detected_and_downsampled_O2]
    s1y_detected_O2 = mockDetections_O2['events']['spin1y'][detected_and_downsampled_O2]
    s1z_detected_O2 = mockDetections_O2['events']['spin1z'][detected_and_downsampled_O2]
    s2x_detected_O2 = mockDetections_O2['events']['spin2x'][detected_and_downsampled_O2]
    s2y_detected_O2 = mockDetections_O2['events']['spin2y'][detected_and_downsampled_O2]
    s2z_detected_O2 = mockDetections_O2['events']['spin2z'][detected_and_downsampled_O2]
    z_detected_O2 = mockDetections_O2['events']['z'][detected_and_downsampled_O2]
    precomputed_lnp_z_O2 = mockDetections_O2['events']['logpdraw_z'][detected_and_downsampled_O2]
    precomputed_lnp_m1_O2 = mockDetections_O2['events']['logpdraw_mass1_source_GIVEN_z'][detected_and_downsampled_O2]
    precomputed_lnp_m2_O2 = mockDetections_O2['events']['logpdraw_mass2_source_GIVEN_mass1_source'][detected_and_downsampled_O2]
    precomputed_p_m1m2z_O2 = np.exp(precomputed_lnp_z_O2+precomputed_lnp_m1_O2+precomputed_lnp_m2_O2)
    run_O2 = np.full(m1_detected_O2.size,'O2')
    print((targetRate/injectionRate_O2),detected_O2.size,detected_and_downsampled_O2.size)

    # And O3!
    ifar_1 = mockDetections_O3['injections']['ifar_gstlal'].value
    ifar_2 = mockDetections_O3['injections']['ifar_pycbc_bbh'].value
    ifar_3 = mockDetections_O3['injections']['ifar_pycbc_hyperbank'].value
    detected_O3 = np.where((ifar_1>ifar_threshold) | (ifar_2>ifar_threshold) | (ifar_3>ifar_threshold))[0]
    detected_and_downsampled_O3 = np.random.choice(detected_O3,size=int(round(detected_O3.size*targetRate/injectionRate_O3)),replace=False)
    m1_detected_O3 = mockDetections_O3['injections']['mass1_source'].value[detected_and_downsampled_O3]
    m2_detected_O3 = mockDetections_O3['injections']['mass2_source'].value[detected_and_downsampled_O3]
    s1x_detected_O3 = mockDetections_O3['injections']['spin1x'].value[detected_and_downsampled_O3]
    s1y_detected_O3 = mockDetections_O3['injections']['spin1y'].value[detected_and_downsampled_O3]
    s1z_detected_O3 = mockDetections_O3['injections']['spin1z'].value[detected_and_downsampled_O3]
    s2x_detected_O3 = mockDetections_O3['injections']['spin2x'].value[detected_and_downsampled_O3]
    s2y_detected_O3 = mockDetections_O3['injections']['spin2y'].value[detected_and_downsampled_O3]
    s2z_detected_O3 = mockDetections_O3['injections']['spin2z'].value[detected_and_downsampled_O3]
    z_detected_O3 = mockDetections_O3['injections']['redshift'].value[detected_and_downsampled_O3]
    precomputed_p_m1m2_O3 = mockDetections_O3['injections']['mass1_source_mass2_source_sampling_pdf'].value[detected_and_downsampled_O3]
    precomputed_p_z_O3 = mockDetections_O3['injections']['redshift_sampling_pdf'].value[detected_and_downsampled_O3]
    precomputed_p_m1m2z_O3 = precomputed_p_m1m2_O3*precomputed_p_z_O3
    run_O3 = np.full(m1_detected_O3.size,'O3')
    print((targetRate/injectionRate_O3),detected_O3.size,detected_and_downsampled_O3.size)

    # Combine
    m1_det = np.concatenate([m1_detected_O3,m1_detected_O1,m1_detected_O2])
    m2_det = np.concatenate([m2_detected_O3,m2_detected_O1,m2_detected_O2])
    s1x_det = np.concatenate([s1x_detected_O3,s1x_detected_O1,s1x_detected_O2])
    s1y_det = np.concatenate([s1y_detected_O3,s1y_detected_O1,s1y_detected_O2])
    s1z_det = np.concatenate([s1z_detected_O3,s1z_detected_O1,s1z_detected_O2])
    s2x_det = np.concatenate([s2x_detected_O3,s2x_detected_O1,s2x_detected_O2])
    s2y_det = np.concatenate([s2y_detected_O3,s2y_detected_O1,s2y_detected_O2])
    s2z_det = np.concatenate([s2z_detected_O3,s2z_detected_O1,s2z_detected_O2])
    z_det = np.concatenate([z_detected_O3,z_detected_O1,z_detected_O2])
    precomputed_p_m1m2z = np.concatenate([precomputed_p_m1m2z_O3,precomputed_p_m1m2z_O1,precomputed_p_m1m2z_O2])
    runs = np.concatenate([run_O3,run_O1,run_O2])

    return m1_det,m2_det,s1x_det,s1y_det,s1z_det,s2x_det,s2y_det,s2z_det,z_det,precomputed_p_m1m2z,runs,nTrials

def genInjectionFile(ifar_threshold,snr_threshold,filename):

    # Load
    m1_det,m2_det,s1x_det,s1y_det,s1z_det,s2x_det,s2y_det,s2z_det,z_det,p_draw_m1m2z,runs,nTrials = loadInjectionsAlt(ifar_threshold,snr_threshold)

    # Derived parameters
    q_det = m2_det/m1_det
    Xeff_det = (m1_det*s1z_det + m2_det*s2z_det)/(m1_det+m2_det)
    Xp_det = calculate_Xp(s1x_det,s1y_det,s1z_det,s2x_det,s2y_det,s2z_det,q_det)
    a1_det = np.sqrt(s1x_det**2 + s1y_det**2 + s1z_det**2)
    a2_det = np.sqrt(s2x_det**2 + s2y_det**2 + s2z_det**2)
    cost1_det = s1z_det/a1_det
    cost2_det = s2z_det/a2_det

    # Compute marginal draw probabilities for chi_effective and joint chi_effective vs. chi_p probabilities
    p_draw_xeff = np.zeros(Xeff_det.size)
    p_draw_xeff_xp = np.zeros(Xeff_det.size)
    for i in range(p_draw_xeff.size):
        if i%500==0:
            print(i)
        p_draw_xeff[i] = chi_effective_prior_from_isotropic_spins(q_det[i],1.,Xeff_det[i])
        p_draw_xeff_xp[i] = joint_prior_from_isotropic_spins(q_det[i],1.,Xeff_det[i],Xp_det[i],ndraws=10000)

    # Combine
    pop_reweight = 1./(p_draw_m1m2z*p_draw_xeff_xp)
    pop_reweight_XeffOnly = 1./(p_draw_m1m2z*p_draw_xeff)
    pop_reweight_noSpin = 1./p_draw_m1m2z

    # Also compute factors of dVdz that we will need to reweight these samples during inference later on
    dVdz = 4.*np.pi*Planck15.differential_comoving_volume(z_det).to(u.Gpc**3*u.sr**(-1)).value

    # Store and save
    injectionDict = {
            'm1':m1_det,
            'm2':m2_det,
            'Xeff':Xeff_det,
            'Xp':Xp_det,
            'z':z_det,
            's1z':s1z_det,
            's2z':s2z_det,
            'a1':a1_det,
            'a2':a2_det,
            'cost1':cost1_det,
            'cost2':cost2_det,
            'dVdz':dVdz,
            'weights':pop_reweight,
            'weights_XeffOnly':pop_reweight_XeffOnly,
            'weights_noSpin':pop_reweight_noSpin,
            'nTrials':nTrials,
            'obsRuns':runs
            }

    with open(filename,"wb") as f:
        pickle.dump(injectionDict,f,protocol=2)

if __name__=="__main__":

    genInjectionFile(1.,10.,'./injectionDict_10-20_directMixture_FAR_1_in_1.pickle')
    genInjectionFile(5.,10.,'./injectionDict_10-20_directMixture_FAR_1_in_5.pickle')
