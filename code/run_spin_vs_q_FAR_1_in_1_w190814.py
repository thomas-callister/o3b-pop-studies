import numpy as np
import glob
import emcee as mc
import h5py
import sys
from support import *
from spin_v_q_likelihood import *

# -- Set prior bounds --
priorDict = {
    'lmbda':(-5,4),
    'mMax':(60,100),
    'm0':(20,100),
    'sigM':(1,10),
    'fPeak':(0,1),
    'bq':(-2,10),
    'sig_kappa':6.,
    'mu0':(-1,1),
    'log_sigma0':(-1.5,0.5),
    'alpha':(-1.5,1),
    'beta':(-2.,1.25),
    'mMin':2.5
    }

# Dicts with samples: 
sampleDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/input/sampleDict_FAR_1_in_1_yr_11-29.pickle")

# Load mock detections
injectionDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/code/injectionDict_10-20_directMixture_FAR_1_in_1.pickle")

nWalkers = 32
output = "/home/thomas.callister/CBC/o3b-pop-studies/results/results_11-29/spin_vs_q_samples_FAR_1_in_1_w190814"

# Search for existing chains
old_chains = np.sort(glob.glob("{0}_r??.npy".format(output)))

# If no chain already exists, begin a new one
if len(old_chains)==0:

    run_version = 0

    # Initialize walkers from random positions in mu-sigma2 parameter space
    initial_lmbdas = np.random.random(nWalkers)*(-0.5)-2.
    initial_mMaxs = np.random.random(nWalkers)*5.+75.
    initial_m0s = np.random.random(nWalkers)*5.+30
    initial_sigMs = np.random.random(nWalkers)*3+1.
    initial_fs = np.random.random(nWalkers)*0.15+0.05
    initial_bqs = np.random.random(nWalkers)*1.+1.
    initial_ks = np.random.normal(size=nWalkers,loc=0,scale=0.5)+2.
    initial_mu0s = np.random.random(nWalkers)*0.05 - 0.1
    initial_sigma0s = np.random.random(nWalkers)*0.5 - 1.
    initial_alphas = np.random.random(nWalkers)*(0.1) - 0.5
    initial_betas = np.random.random(nWalkers)*(0.1) - 0.5
    initial_walkers = np.transpose([initial_lmbdas,initial_mMaxs,initial_m0s,initial_sigMs,initial_fs,initial_bqs,initial_ks,initial_mu0s,initial_sigma0s,initial_alphas,initial_betas])

# Otherwise resume existing chain
else:

    # Load existing file and iterate run version
    old_chain = np.load(old_chains[-1])
    run_version = int(old_chains[-1][-6:-4])+1

    # Strip off any trailing zeros due to incomplete run
    goodInds = np.where(old_chain[0,:,0]!=0.0)[0]
    old_chain = old_chain[:,goodInds,:]

    # Initialize new walker locations to final locations from old chain
    initial_walkers = old_chain[:,-1,:]

print('Initial walkers:')
print(initial_walkers)

# Dimension of parameter space
dim = 11

# Run
nSteps = 20000
sampler = mc.EnsembleSampler(nWalkers,dim,logp_powerLawPeak,args=[sampleDict,injectionDict,priorDict],threads=16)
for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
    if i%10==0:
        np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
