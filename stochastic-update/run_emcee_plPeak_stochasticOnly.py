import numpy as np
import glob
import emcee as mc
import h5py
from scipy.stats import gaussian_kde
from scipy.special import erf
from scipy.io import loadmat
from astropy.cosmology import Planck15
import astropy.units as u
import sys
sys.path.append('/home/thomas.callister/Stochastic/stochastic-modeling/code/')
from gwBackground import *

# Total observing time
mockDetections_O1 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o1-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
mockDetections_O2 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o2-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
mockDetections_O3 = h5py.File('/home/reed.essick/rates+pop/o3-sensitivity-estimates/LIGO-T2100113-v10/endo3_bbhpop-LIGO-T2100113-v10.hdf5','r')
Tobs_O1 = mockDetections_O1.attrs['total_analysis_time']  
Tobs_O2 = mockDetections_O2.attrs['total_analysis_time']
Tobs_O3 = mockDetections_O3.attrs['analysis_time_s'] 
Tobs = (Tobs_O1+Tobs_O2+Tobs_O3)/(365.*24.*3600.)

######################
# Set prior bounds 
######################

xeff_mu_min = -0.2
xeff_mu_max = 0.4
xeff_sig_min = 0.01
xeff_sig_max = 0.5
R0_min = 0.1
R0_max = 1000.
mMin_min = 2
mMin_max = 10
mMax_min  = 70.
mMax_max = 100.
lmbda_min = -8
lmbda_max = 4
mu_peak_min = 20
mu_peak_max = 45
sig_peak_min = 1
sig_peak_max = 10
bq_min = -2
bq_max = 10
alpha_min = -10.
alpha_max = 25.
beta_min = 0.
beta_max = 10.
zpeak_min = 0.
zpeak_max = 4.

################################
# Load preprocessed BBH samples,
# precompute dVdz
################################

# Overwrite cosmology
Planck15_mod = Planck15.clone(name='Planck15 modified',Om0=0.3065,H0=67.9,Tcmb0=0.)
print("Omega_m:\t{0}".format(Planck15_mod.Om0))
print("Omega_L:\t{0}".format(Planck15_mod.Ode0))
print("H0:\t{0}".format(Planck15_mod.H0))

def dVdz(z):
    return 4.*np.pi*Planck15_mod.differential_comoving_volume(z).to(u.Gpc**3/u.sr).value

##################
# Redshift data
##################

zMax = 2.3
all_zs = np.linspace(0,10,1000)
all_dVc_dz = dVdz(all_zs)

############################
# Preprocess stochastic data
############################

# Import stochastic data
# Cf: Measured cross-correlation spectrum
# sigma: Estimated standard deviation of estimate as a function of frequency
matdata = loadmat("/home/thomas.callister/Stochastic/o3-isotropic/runs/combine_results/full_combined_results_alpha0.mat")
Cf = np.array(matdata['ptEst_ff']).reshape(-1)
sigmas = np.array(matdata['sigma_ff']).reshape(-1)
freqs = np.array(matdata['freq']).reshape(-1)

# Select frequencies below 300 Hz
lowFreqs = freqs<300.
freqs = freqs[lowFreqs]
Cf = Cf[lowFreqs]
sigma2s = sigmas[lowFreqs]**2.

# Select only frequencies with data
# This step removes frequency bins that have been notched due to the presence of
# loud or unsafe lines
goodInds = np.where(Cf==Cf)
freqs = freqs[goodInds]
Cf = Cf[goodInds]
sigma2s = sigma2s[goodInds]

# Prepare OmegaGW calculator object
# Arguments here are the minimum/maximum mass bounds to consider (the mass distribution
# between these bounds will be reweighted according to our various proposed hyperparameters
# below) and a grid of redshifts across which we will integrate
omg = OmegaGW_BBH(2.,100.,all_zs)

# -- Log posterior function -- 
def logposterior(c):

    logP = 0.

    # Read parameters
    xeff_mu = c[0]
    xeff_sig = c[1]
    R0 = c[2]
    mMin = c[3]
    mMax = c[4]
    lmbda = c[5]
    mu_peak = c[6]
    sig_peak = c[7]
    frac_peak = c[8]
    bq = c[9]
    alpha = c[10]
    beta = c[11]
    zpeak = c[12]

    # Flat priors, reject samples past boundaries
    if xeff_mu<xeff_mu_min or xeff_mu>xeff_mu_max or xeff_sig<xeff_sig_min or xeff_sig>xeff_sig_max or R0<R0_min or R0>R0_max or mMin<mMin_min or mMin>mMin_max or mMax<mMax_min or mMax>mMax_max or lmbda<lmbda_min or lmbda>lmbda_max or mu_peak<mu_peak_min or mu_peak>mu_peak_max or sig_peak<sig_peak_min or sig_peak>sig_peak_max or frac_peak<0 or frac_peak>1 or bq<bq_min or bq>bq_max or alpha<alpha_min or alpha>alpha_max or beta<beta_min or beta>beta_max or zpeak<zpeak_min or zpeak>zpeak_max:
        return -np.inf

    # If sample in prior range, evaluate
    else:

        ################################
        # Poisson contribution from rate
        ################################

        # First, this array is the (unnormalized) functional form for R(z).
        # In particular, this array is R(z)/(C*R(0)), where C is a normalization constant
        # and R(0) is the local merger rate density; see Eq. 15 in the paper
        rate_tmp = np.power(1.+all_zs,alpha)/(1.+np.power((1.+all_zs)/(1.+zpeak),alpha+beta))
        
        logP += - np.log(R0)


        ##########################
        # Stochastic contribution
        ##########################

        # Plug in the proposed mass distribution
        # This serves to reweight energy-densities that have been precomputed across
        # a grid of (M_tot,q) values
        omg.setProbs_plPeak(mMin,mMax,lmbda,mu_peak,sig_peak,frac_peak,bq)

        # Plug in the proposed local rate density as well as the proposed *shape* of R(z).
        # These are combined inside the energy-density object via R0*rate_tmp/rate_tmp[0]
        # to build a properly-normalized R(z)
        Omega_f = omg.eval(R0,rate_tmp,freqs)
        print(Omega_f)

        # Compute the Gaussian likelihood for Cf and add onto running log-probability
        diff = Omega_f-Cf
        logP_stoch = np.sum(-0.5*diff*diff/sigma2s)
        logP += logP_stoch

        return logP
    
# -- Running mcmc --     
if __name__=="__main__":

    # Define output file
    output = "/home/thomas.callister/CBC/o3b-pop-studies/stochastic-update/results_11-29/emcee_samples_stochasticOnly"
    nWalkers = 32
    
    # Search for existing chains
    old_chains = np.sort(glob.glob("{0}_r??.npy".format(output)))

    # If no chain already exists, begin a new one
    if len(old_chains)==0:

        run_version = 0

        # Initialize walkers from random positions in mu-sigma2 parameter space
        initial_mus = np.random.random(nWalkers)*0.1
        initial_sigs = np.random.random(nWalkers)*0.1+0.1
        initial_R0s = 10.**(4.*np.random.random(nWalkers)-1.)
        initial_mMins = np.random.random(nWalkers)*0.5+2.
        initial_mMaxs = np.random.random(nWalkers)*20.+70.
        initial_lmbdas = np.random.random(nWalkers)*(-3.)
        initial_mu_peaks = np.random.random(nWalkers)*10+30.
        initial_sig_peaks = np.random.random(nWalkers)*3.+2.
        initial_frac_peaks = np.random.random(nWalkers)
        initial_bqs = np.random.random(nWalkers)*3.
        initial_alphas = np.random.random(nWalkers)*2.
        initial_betas = np.random.random(nWalkers)*(beta_max-beta_min) + beta_min
        initial_zpeaks = np.random.random(nWalkers)*(zpeak_max-zpeak_min) + zpeak_min
        initial_walkers = np.transpose([initial_mus,initial_sigs,initial_R0s,initial_mMins,initial_mMaxs,initial_lmbdas,initial_mu_peaks,initial_sig_peaks,initial_frac_peaks,initial_bqs,initial_alphas,initial_betas,initial_zpeaks])

    # Otherwise resume existing chain
    else:

        # Load existing file and iterate run version
        old_chain = np.load(old_chains[-1])
        run_version = int(old_chains[-1][-6:-4])+1

        # Strip off any trailing zeros due to incomplete run
        goodInds = np.where(old_chain[0,:,0]!=0.0)[0]
        old_chain = old_chain[:,goodInds,:]

        # Initialize new walker locations to final locations from old chain
        initial_walkers = old_chain[:,-1,:]
    
    # Dimension of parameter space
    dim = 13

    # Run
    nSteps = 50000
    sampler = mc.EnsembleSampler(nWalkers,dim,logposterior,threads=16)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        if i%50==0:
            np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
    np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
