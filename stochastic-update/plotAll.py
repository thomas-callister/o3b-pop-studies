import matplotlib.pyplot as plt
import sys
import numpy as np
from tqdm import tqdm
from matplotlib.patches import Rectangle

# Load BNS data
bnsData = np.load('omegaGW_savedBNS.npy')[()]
bnsBackgrounds = bnsData['OmgGW']
freqs = bnsData['f']
bns_omg_05 = np.quantile(bnsBackgrounds,0.05,axis=0)
bns_omg_50 = np.quantile(bnsBackgrounds,0.50,axis=0)
bns_omg_95 = np.quantile(bnsBackgrounds,0.95,axis=0)

# NSBH data
nsbhData = np.load('omegaGW_savedNSBH.npy')[()]
nsbhBackgrounds = nsbhData['OmgGW']
nsbh_freqs = nsbhData['f']
nsbh_omg_05 = np.quantile(nsbhBackgrounds,0.05,axis=0)
nsbh_omg_50 = np.quantile(nsbhBackgrounds,0.50,axis=0)
nsbh_omg_95 = np.quantile(nsbhBackgrounds,0.95,axis=0)
print(np.interp(25.,nsbh_freqs,nsbh_omg_50))
print(np.interp(25.,nsbh_freqs,nsbh_omg_95-nsbh_omg_50))
print(np.interp(25.,nsbh_freqs,nsbh_omg_50-nsbh_omg_05))

# BBH data
bbhData = np.load('omegaGW_savedBBH.npy')[()]
bbhBackgrounds = bbhData['OmgGW']
bbh_freqs = bbhData['f']
bbh_omg_05 = np.quantile(bbhBackgrounds,0.05,axis=0)
bbh_omg_50 = np.quantile(bbhBackgrounds,0.50,axis=0)
bbh_omg_95 = np.quantile(bbhBackgrounds,0.95,axis=0)

# Sanity check: make sure frequency arrays agree
if (freqs!=nsbh_freqs).any() or (freqs!=bbh_freqs).any():
    print("Frequency arrays don't match!")
    sys.exit()

# Total number of spectra generated
n_bns = bnsBackgrounds.shape[0]
n_nsbh = nsbhBackgrounds.shape[0]
n_bbh = bbhBackgrounds.shape[0]

# Get quantiles describing total Omega(f)
net_05 = np.zeros(freqs.size)
net_50 = np.zeros(freqs.size)
net_95 = np.zeros(freqs.size)
for i in tqdm(range(freqs.size)):

    # At each frequency, draw many combinations of OmegaBBH+OmegaBNS+OmegaNSBH 
    bns = bnsBackgrounds[np.random.choice(range(0,n_bns),size=10000,replace=True),i]
    nsbh = nsbhBackgrounds[np.random.choice(range(0,n_nsbh),size=10000,replace=True),i]
    bbh = bbhBackgrounds[np.random.choice(range(0,n_bbh),size=10000,replace=True),i]

    # Compute an ensemble of possible totals
    total = bns+bbh+nsbh

    # Record quantiles
    net_05[i] = np.quantile(total,0.05)
    net_50[i] = np.quantile(total,0.50)
    net_95[i] = np.quantile(total,0.95)

print(np.interp(25.,freqs,net_50))
print(np.interp(25.,freqs,net_95-net_50))
print(np.interp(25.,freqs,net_50-net_05))

fig = plt.figure(figsize=(7.5,3))

######################################
# Plot A: Individual CBC contributions
######################################

ax = fig.add_subplot(121)
ax.set_xlim(10,1000)
ax.set_ylim(3e-12,1e-7)
ax.xaxis.grid(True,which='major',ls=':',color='grey')
ax.yaxis.grid(True,which='major',ls=':',color='grey')
ax.set_xlabel(r'$f\,(\mathrm{Hz})$',fontsize=10)
ax.set_ylabel(r'$\Omega_\mathrm{GW}(f)$',fontsize=10)
ax.set_xscale('log')
ax.set_yscale('log')

# Plot individual contributions
ax.fill_between(freqs,bbh_omg_05,bbh_omg_95,facecolor='#33a02c',alpha=0.6,lw=0.5,zorder=3,edgecolor='black',label='BBH')
ax.fill_between(freqs,bns_omg_05,bns_omg_95,facecolor='#e31a1c',alpha=0.6,lw=0.5,zorder=1,edgecolor='black',label='BNS')
ax.fill_between(freqs,nsbh_omg_05,nsbh_omg_95,facecolor='#33eff5',alpha=0.8,lw=0.5,zorder=2,edgecolor='black',label='NSBH')

ax.legend(loc='upper left',fontsize=9,frameon=False)

##############################
# Plot B: Totals
##############################

ax = fig.add_subplot(122)
ax.set_xlim(10,1000)
ax.set_ylim(3e-12,1e-7)
ax.xaxis.grid(True,which='major',ls=':',color='grey')
ax.yaxis.grid(True,which='major',ls=':',color='grey')
ax.set_xlabel(r'$f\,(\mathrm{Hz})$',fontsize=10)
ax.set_xscale('log')
ax.set_yscale('log')
ax.yaxis.set_ticklabels([])

# Plot uncertainty range on totals
ax.fill_between(freqs,net_05,net_95,facecolor='#33eff5',edgecolor=None,lw=0,alpha=0.8)
lc, = ax.plot(freqs,net_50,color='#1f78b4')

# Load PI curve data
#freqs_O1O2,PI_O1O2 = np.loadtxt('/home/thomas.callister/Stochastic/o3-isotropic/runs/combine_results/O1O2PICurve.dat',unpack=True,skiprows=1,usecols=(0,2))
freqs_O3,PI_O3 = np.loadtxt('/home/thomas.callister/Stochastic/o3-isotropic/runs/combine_results/O3PICurve.dat',unpack=True,skiprows=3,usecols=(0,2))
freqs_design,PI_design = np.loadtxt('/home/thomas.callister/Stochastic/o3-isotropic/projections/PICurves/Design_HLV_flow_10.txt',unpack=True,usecols=(0,1))
freqs_aplus,PI_aplus = np.loadtxt('/home/thomas.callister/Stochastic/o3-isotropic/projections/PICurves/Design_Aplus_flow_10_with_Virgo.txt',unpack=True,usecols=(0,1))

# Plot 2sigma PI curves
#ax.plot(freqs_O1O2,2.*PI_O1O2,dashes=(1,1),label='O2',zorder=1,lw=1.2,color='#5e5e5e')
ax.plot(freqs_O3,2.*PI_O3,color='black',label='O3 Sensitivity',zorder=1)
ax.plot(freqs_design,2.*PI_design,color='#5e5e5e',dashes=(2,1.5),label='Design HLV',lw=1.2,zorder=1)
ax.plot(freqs_aplus,2.*PI_aplus,color='#5e5e5e',dashes=(3,1,1,1),label='Design A+',lw=1.2,zorder=1)

alt_legend = plt.legend([lc],['Total Background'],fontsize=9,frameon=False,loc=(0.02,0.06))
ax.add_artist(alt_legend)
ax.legend(loc=(0.58,0.06),fontsize=9,frameon=False,handlelength=1.5)
rect = Rectangle((0.033,0.072),0.089,0.075,facecolor="#33eff5",zorder=1,transform=ax.transAxes)
ax.add_patch(rect)

plt.tight_layout()
plt.savefig('./backgroundPlot.pdf',bbox_inches='tight')

