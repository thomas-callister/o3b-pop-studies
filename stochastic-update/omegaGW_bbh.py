import sys
sys.path.append('/home/thomas.callister/Stochastic/stochastic-modeling/code/')
from gwBackground import *
from scipy.integrate import trapz
from scipy.integrate import cumtrapz
import matplotlib.pyplot as plt
from scipy.special import gammainc
from tqdm import tqdm

###########################
# Define redshift evolution
###########################

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('/home/thomas.callister/Stochastic/stochastic-modeling/code/delayedRateData.npy',allow_pickle=True,encoding='latin1')[()]
zsMerger = rateData['zs']                       # Array (M) of merger redshifts
print(zsMerger)
print(zsMerger.size)
tdelays = rateData['tds']                       # Array (N) of time delays between redshift and merger
formationRates = rateData['formationRates']     # MxN array of *formation* redshifts corresponding to each combination of (z_merge,t_d)
zsFormation = rateData['formationRedshifts']    # MxN array containing values of SFR at each formation redshift

# Weight formation rates by the fraction of star formation at metallicities Z<0.1Zsun
# See Langer+Normal 2006 (astro-ph/0512271)
fs = gammainc(0.84,(0.1**2.)*np.power(10.,0.3*zsFormation))
weightedFormationRates = formationRates*fs

# Convolve formation rate with time-delay distribution
# Set t_min = 50 Myr
tdMin = 0.050
dpdt = np.power(tdelays,-1)
dpdt[tdelays<tdMin] = 0.
mergerRate = weightedFormationRates.dot(dpdt)

##########################
# Compute energy densities
##########################

# Initialize helper object
omg = OmegaGW_BBH(2.,100.,zsMerger)
freqs = np.logspace(1,3.5,200)

# Extract posterior samples on local rate and mass distribution
samps = np.load('results_11-29/processed_emcee_samples_bbh_fixedRedshiftEvolution_nEffCut_r00.npy')
spectra = np.zeros((1000,freqs.size))

# Loop across 1000 random samples
randomInds = np.random.choice(range(samps.shape[0]),size=1000)
for i,ind in tqdm(enumerate(randomInds),total=1000):

    # Unpack posterio rsample
    R0 = samps[ind,0]
    mMin = samps[ind,1]
    mMax = samps[ind,2]
    lmbda = samps[ind,3]
    mu_peak = samps[ind,4]
    sig_peak = samps[ind,5]
    frac_peak = samps[ind,6]
    bq = samps[ind,7]

    # Define mass weights and generate Omega(f)
    omg.setProbs_plPeak(mMin,mMax,lmbda,mu_peak,sig_peak,frac_peak,bq)
    spec = omg.eval(R0,mergerRate/mergerRate[0],freqs)
    spectra[i,:] = spec

# Save
backgroundData = {'OmgGW':spectra,'f':freqs}
np.save("results_11-29/omegaGW_savedBBH.npy",backgroundData)

print(np.quantile(spectra,0.5,axis=0)[np.argmin(np.abs(freqs-25.))])
print(np.quantile(spectra,0.95,axis=0)[np.argmin(np.abs(freqs-25.))])
print(np.quantile(spectra,0.05,axis=0)[np.argmin(np.abs(freqs-25.))])

fig,ax = plt.subplots()
for i in range(spectra[:,0].size):
    ax.plot(freqs,spectra[i,:],color='blue',alpha=0.2,lw=0.3)
ax.plot(freqs,np.median(spectra,axis=0),color='black')
ax.plot(freqs,np.quantile(spectra,0.05,axis=0),color='black')
ax.plot(freqs,np.quantile(spectra,0.95,axis=0),color='black')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylim(1e-11,1e-7)
ax.set_xlim(10,1000)
ax.set_xlabel(r'$f$ (Hz)',fontsize=14)
ax.set_ylabel(r'$\Omega(f)$',fontsize=14)
ax.xaxis.grid(True,which='major',ls='--',color='grey')
ax.yaxis.grid(True,which='major',ls='--',color='grey')
plt.savefig('bbh_background_wSpin_resubmission.pdf')
