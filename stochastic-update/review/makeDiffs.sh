#!/bin/bash

diff -yt ../run_emcee_bbh_fixedRedshiftEvolution.py /home/thomas.callister/Stochastic/o3-isotropic/cbc-implications/bbh-pop/run_emcee.py > run_emcee_bbh_diff.txt
diff -yt ../omegaGW_bbh.py /home/thomas.callister/Stochastic/o3-isotropic/cbc-implications/bbh-pop/omegaGW.py > omegaGW_bbh_diff.txt
diff -yt ../omegaGW_bns.py /home/thomas.callister/Stochastic/o3-isotropic/cbc-implications/bns-pop/omegaGW.py > omegaGW_bns_diff.txt
diff -yt ../run_emcee_plPeak.py /home/thomas.callister/Stochastic/o3-isotropic/cbc-implications/rate-constraints/run_emcee.py > run_emcee_Rz_diff.txt
