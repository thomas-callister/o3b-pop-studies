import sys
sys.path.append('/home/thomas.callister/Stochastic/stochastic-modeling/code/')
from gwBackground import *
from scipy.integrate import trapz
from scipy.integrate import cumtrapz
from scipy.special import erfinv
from tqdm import tqdm
import json

###########################
# Define redshift evolution
###########################

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('/home/thomas.callister/Stochastic/stochastic-modeling/code/delayedRateData.npy',allow_pickle=True,encoding='latin1')[()]
zsMerger = rateData['zs']                       # Array (M) of merger redshifts 
tdelays = rateData['tds']                       # Array (N) of time delays between redshift and merger
formationRates = rateData['formationRates']     # MxN array of *formation* redshifts corresponding to each combination of (z_merge,t_d)
zsFormation = rateData['formationRedshifts']    # # MxN array containing values of SFR at each formation redshift  

# Convolve formation rate with time-delay distribution
# Set t_min = 20 Myr
tdMin = 0.02
dpdt = np.power(tdelays,-1)
dpdt[tdelays<tdMin] = 0.
mergerRate = formationRates.dot(dpdt)

###########################
# Define local merger rate
###########################

bns_rate_file = "/home/thomas.callister/CBC/o3b-population-data/postproc/simpleBNSrate.json"
with open(bns_rate_file) as jf:
    rate_data = json.load(jf)

logR_mean = np.log10(rate_data['median'])
logR_measured95 = np.log10(rate_data['95th percentile'])
logR_measured05 = np.log10(rate_data['5th percentile'])

# Assume a log-normal form for these measurements.
# For each one-sided error, compute the implicit standard deviation
logR_sigma_upper = (logR_measured95-logR_mean)/(np.sqrt(2.)*erfinv(2.*0.95-1))
logR_sigma_lower = (logR_measured05-logR_mean)/(np.sqrt(2.)*erfinv(2.*0.05-1))
print("Upper and lower standard deviations:",logR_sigma_upper,logR_sigma_lower)

# Average the two one-sided uncertainties and draw rate samples
logR_sigma = (logR_sigma_upper+logR_sigma_lower)/2.
rate_samples = 10.**np.random.normal(size=1000,loc=logR_mean,scale=logR_sigma)

####################
# Compute Omega(f)
####################

# Initialize helper object
omg = OmegaGW_BNS(1,2.5,zsMerger)
freqs = np.logspace(1,3.5,200)

# Loop across random rates, evaluate and store background
backgrounds = np.zeros((rate_samples.size,freqs.size))
for i,r in tqdm(enumerate(rate_samples),total=1000):
    spec = omg.eval(r,mergerRate,freqs)
    backgrounds[i,:] = spec

omg_25 = np.zeros(1000)
for i in range(1000):
    omg_25[i] = np.interp(25.,freqs,backgrounds[i,:])
print(np.median(omg_25))

backgroundData = {'OmgGW':backgrounds,'f':freqs}
np.save("results_11-29/omegaGW_savedBNS.npy",backgroundData)
