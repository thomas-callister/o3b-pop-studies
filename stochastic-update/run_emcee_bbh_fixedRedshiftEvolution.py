import numpy as np
import glob
import emcee as mc
import h5py
from scipy.stats import gaussian_kde
from scipy.special import erf
import sys
from astropy.cosmology import Planck15
import astropy.units as u
from scipy.special import gammainc
sys.path.append('/home/thomas.callister/Stochastic/stochastic-modeling/code/')
from gwBackground import *

# Total observing time
mockDetections_O1 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o1-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
mockDetections_O2 = h5py.File('/home/reed.essick/rates+pop/o1+o2-sensitivity-estimates/LIGO-T2100280-v2/o2-bbh-IMRPhenomXPHMpseudoFourPN.hdf5','r')
mockDetections_O3 = h5py.File('/home/reed.essick/rates+pop/o3-sensitivity-estimates/LIGO-T2100113-v10/endo3_bbhpop-LIGO-T2100113-v10.hdf5','r')
Tobs_O1 = mockDetections_O1.attrs['total_analysis_time']  
Tobs_O2 = mockDetections_O2.attrs['total_analysis_time']
Tobs_O3 = mockDetections_O3.attrs['analysis_time_s'] 
Tobs = (Tobs_O1+Tobs_O2+Tobs_O3)/(365.25*24.*3600.)


####################
# Set prior bounds
####################

R0_min = 0.1
R0_max = 1000.
mMin_min = 2
mMin_max = 10
mMax_min  = 70.
mMax_max = 100.
lmbda_min = -8
lmbda_max = 4
mu_peak_min = 20
mu_peak_max = 45
sig_peak_min = 1
sig_peak_max = 10
bq_min = -2
bq_max = 10
xeff_mu_min = -0.5
xeff_mu_max = 0.5
xeff_sig_min = 0.05
xeff_sig_max = 0.5

################################
# Load preprocessed BBH samples,
# precompute dVdz
################################

# Overwrite cosmology
Planck15_mod = Planck15.clone(name='Planck15 modified',Om0=0.3065,H0=67.9,Tcmb0=0.)
print("Omega_m:\t{0}".format(Planck15_mod.Om0))
print("Omega_L:\t{0}".format(Planck15_mod.Ode0))
print("H0:\t{0}".format(Planck15_mod.H0))

# Helper function to calculate differential comoving volume
def dVdz(z):
    return 4.*np.pi*Planck15_mod.differential_comoving_volume(z).to(u.Gpc**3/u.sr).value

# Dicts with samples: 
sampleDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/input/sampleDict_FAR_1_in_1_yr_11-29.pickle")
sampleDict.pop('S190814bv')
for key in sampleDict:
    dVdz_samps = dVdz(sampleDict[key]['z'])
    sampleDict[key]['dVdz'] = dVdz_samps

##########################
# Redshift evolution model
##########################

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('/home/thomas.callister/Stochastic/stochastic-modeling/code/delayedRateData.npy',allow_pickle=True,encoding='latin1')[()]
zsMerger = rateData['zs']                       # 1D array (size M) of redshifts at merger
tdelays = rateData['tds']                       # 1D array (size N) of time delays between formation and merger
zsFormation = rateData['formationRedshifts']    # 2D array (MxN) of redshifts at *formation* as function of time delay and merger z
formationRates = rateData['formationRates']     # 2D array (MxN) of corresponding SFR values at formation redshifts

# Metallicity weights, assuming BBH formation at <0.1 Zsun
# See Langer & Normal 2006 (10.1086/500363)
fs = gammainc(0.84,(0.1**2.)*np.power(10.,0.3*zsFormation))
weightedFormationRates = formationRates*fs

# Convolve formation rate with time-delay distribution
zMax = 2.3
tdMin = 0.050
dpdt = np.power(tdelays,-1)
dpdt[tdelays<tdMin] = 0.
dpdt[tdelays>13.5] = 0.
mergerRate = weightedFormationRates.dot(dpdt)

# Normalize and restrict to the redshift range spanned by injections
mergerRate /= mergerRate[0]
rateDensity_unnorm = mergerRate[zsMerger<=zMax]
z_grid = zsMerger[zsMerger<=zMax]

# On this same redshift grid, precompute differential comoving volumes
dVdz_grid = dVdz(z_grid)

# Precompute p(z) for each of our posterior samples
for key in sampleDict:
    z_samps = sampleDict[key]['z']
    dVdz_samps = sampleDict[key]['dVdz']
    pz_samps = dVdz_samps*np.interp(z_samps,z_grid,rateDensity_unnorm)*(1./(1.+z_samps))/np.trapz(dVdz_grid*rateDensity_unnorm/(1.+z_grid),z_grid)
    sampleDict[key]['pz'] = pz_samps

########################
# Load mock detections
########################

injectionDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/code/injectionDict_10-20_directMixture_FAR_1_in_1.pickle")
nTrials = injectionDict['nTrials']
m1_det = injectionDict['m1']
m2_det = injectionDict['m2']
z_det = injectionDict['z']
X_det = injectionDict['Xeff']
pop_reweight = injectionDict['weights_XeffOnly']
dVdz_det = injectionDict['dVdz']

# Define *target* redshift distribution, and (partially complete) reweighting factors to use below
p_det_z = dVdz_det*np.interp(z_det,z_grid,rateDensity_unnorm)*(1./(1.+z_det))/np.trapz(dVdz_grid*rateDensity_unnorm/(1.+z_grid),z_grid)

# Function to evaluate truncated Gaussian for spin sector
def Calculate_Gaussian(x, mu, sigma2, low, high): 
    norm = np.sqrt(sigma2*np.pi/2)*(-erf((low-mu)/np.sqrt(2*sigma2)) + erf((high-mu)/np.sqrt(2*sigma2)))
    y = (1.0/norm)*np.exp((-1.0*(x-mu)**2)/(2.*sigma2)) 
    y[np.where(x<low)] = 0.
    return y

def plPeakDistribution(m1,lmbda,mu_peak,sig_peak,mMin,mMax,frac_peak):
    p_m1_pl = (1.+lmbda)*m1**lmbda/(mMax**(1.+lmbda) - mMin**(1.+lmbda))
    p_m1_peak = Calculate_Gaussian(m1,mu_peak,sig_peak**2,mMin,mMax)
    return frac_peak*p_m1_peak + (1.-frac_peak)*p_m1_pl

# -- Log posterior function -- 
def logposterior(c):

    # Read parameters
    R0 = c[0]
    mMin = c[1]
    mMax = c[2]
    lmbda = c[3]
    mu_peak = c[4]
    sig_peak = c[5]
    frac_peak = c[6]
    bq = c[7]
    xeff_mu = c[8]
    xeff_sig = c[9]

    # Flat priors, reject samples past boundaries
    if R0<R0_min or R0>R0_max or mMin<mMin_min or mMin>mMin_max or mMax<mMax_min or mMax>mMax_max or lmbda<lmbda_min or lmbda>lmbda_max or mu_peak<mu_peak_min or mu_peak>mu_peak_max or sig_peak<sig_peak_min or sig_peak>sig_peak_max or frac_peak<0 or frac_peak>1 or bq<bq_min or bq>bq_max or xeff_mu<xeff_mu_min or xeff_mu>xeff_mu_max or xeff_sig<xeff_sig_min or xeff_sig>xeff_sig_max:
        return -np.inf

    # If sample in prior range, evaluate
    else:

        logP = 0.

        #############################
        # Compute detection fraction
        #############################

        # First compute proposed p(m2|m1) for each found injection
        p_det_m2 = (1.+bq)*np.power(m2_det,bq)/(np.power(m1_det,1.+bq)-mMin**(1.+bq))
        p_det_m2[m2_det<mMin] = 0.

        # Next compute p(m1) for found injections
        p_det_m1 = plPeakDistribution(m1_det,lmbda,mu_peak,sig_peak,mMin,mMax,frac_peak)
        p_det_m1[m1_det>mMax] = 0

        # Finally, p(xeff)
        p_det_xeff = Calculate_Gaussian(X_det,xeff_mu,xeff_sig**2.,-1.,1.)

        # Construct full reweighting factors and sum to obtain detection fraction
        det_weights = p_det_m1*p_det_m2*p_det_xeff*p_det_z*pop_reweight/nTrials
        dFraction = np.sum(det_weights)

        # Check effective number of samples
        nEvents = len(sampleDict)
        Nsamp = np.sum(det_weights)**2/np.sum(det_weights**2)
        if Nsamp<=4*nEvents:
            print("Insufficient mock detections:",c)
            return -np.inf

        # Otherwise add to log-probability!
        logP += -nEvents*np.log(dFraction)

        #####################
        # Individual BBHs
        #####################

        # Now loop across each individual event
        for event in sampleDict:

            # Grab samples
            m1_sample = sampleDict[event]['m1']
            m2_sample = sampleDict[event]['m2']
            z_sample = sampleDict[event]['z']
            x_sample = sampleDict[event]['Xeff']
            x_priors = sampleDict[event]['Xeff_priors']
            dVdz_sample = sampleDict[event]['dVdz']
            weights = sampleDict[event]['weights']

            # p(m1)
            p_m1 = plPeakDistribution(m1_sample,lmbda,mu_peak,sig_peak,mMin,mMax,frac_peak)
            p_m1[m1_sample>mMax] = 0
            
            # p(m2|m1)
            p_m2 = (1.+bq)*np.power(m2_sample,bq)/(np.power(m1_sample,1.+bq)-mMin**(1.+bq))
            p_m2[m2_sample<mMin]=0

            # p(xeff)
            p_x = Calculate_Gaussian(x_sample,xeff_mu,xeff_sig**2.,-1.,1.)

            # Read out p(z)
            p_z = sampleDict[event]['pz']
            old_p_z = dVdz_sample*np.power(1.+z_sample,2.7-1.)

            # Evaluate marginalized likelihood
            integrand = p_m1*p_m2*p_z*p_x*weights/x_priors/old_p_z
            neff_per_event = np.sum(integrand)**2/np.sum(integrand**2)
            if neff_per_event<10:
                print("Rejecting",mMin,event,neff_per_event)
                return -np.inf

            integrand = p_m1*p_m2*p_z*p_x*weights/x_priors/old_p_z
            nEff_per_event = np.sum(integrand)**2/np.sum(integrand**2)
            if nEff_per_event<10:
                return -np.inf

            nSamples = p_m1.size
            pEvidence = np.sum(p_m1*p_m2*p_z*p_x*weights/x_priors/old_p_z)/nSamples
            
            # Summation
            logP += np.log(pEvidence)

        ################################
        # Poisson contribution from rate
        ################################

        # Finally, take care of the overall rate
        # First, construct distribution of observed redshifts.
        # Note that this is R(z)/R(0), rather than a probability distribution.
        pz_unnormed = dVdz_grid*rateDensity_unnorm/(1.+z_grid)

        # Integrating across redshifts and multiplying by an observing time gives a total number of events
        pz_integral = np.trapz(pz_unnormed,z_grid)
        Ntot = R0*Tobs*pz_integral

        # Add Poisson term, with a 1/R0 prior on the local merger rate
        logP += nEvents*np.log(Ntot*dFraction) - Ntot*dFraction - np.log(Ntot)

        print(R0,logP)
        return logP
    
# -- Running mcmc --     
if __name__=="__main__":

    # Define output file
    output = "/home/thomas.callister/CBC/o3b-pop-studies/stochastic-update/results_11-29/emcee_samples_bbh_fixedRedshiftEvolution_nEffCut"
    nWalkers = 32
    
    # Search for existing chains
    old_chains = np.sort(glob.glob("{0}_r??.npy".format(output)))

    # If no chain already exists, begin a new one
    if len(old_chains)==0:

        run_version = 0

        # Initialize walkers from random positions in mu-sigma2 parameter space
        initial_R0s = np.random.random(nWalkers)*10.+10.
        initial_mMins = np.random.random(nWalkers)*0.5+2.
        initial_mMaxs = np.random.random(nWalkers)*20.+70.
        initial_lmbdas = np.random.random(nWalkers)*(-3.)
        initial_mu_peaks = np.random.random(nWalkers)*10+30.
        initial_sig_peaks = np.random.random(nWalkers)*3.+2.
        initial_frac_peaks = np.random.random(nWalkers)
        initial_bqs = np.random.random(nWalkers)*3.
        initial_mus = np.random.random(nWalkers)*0.1
        initial_sigs = np.random.random(nWalkers)*0.1+0.1
        initial_walkers = np.transpose([initial_R0s,initial_mMins,initial_mMaxs,initial_lmbdas,initial_mu_peaks,initial_sig_peaks,initial_frac_peaks,initial_bqs,initial_mus,initial_sigs])

    # Otherwise resume existing chain
    else:

        # Load existing file and iterate run version
        old_chain = np.load(old_chains[-1])
        run_version = int(old_chains[-1][-6:-4])+1

        # Strip off any trailing zeros due to incomplete run
        goodInds = np.where(old_chain[0,:,0]!=0.0)[0]
        old_chain = old_chain[:,goodInds,:]

        # Initialize new walker locations to final locations from old chain
        initial_walkers = old_chain[:,-1,:]
    
    # Dimension of parameter space
    dim = 10

    # Run
    nSteps = 30000
    sampler = mc.EnsembleSampler(nWalkers,dim,logposterior,threads=16)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        if i%50==0:
            np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
    np.save("{0}_r{1:02d}.npy".format(output,run_version),sampler.chain)
