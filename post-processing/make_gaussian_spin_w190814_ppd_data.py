import numpy as np
from tqdm import tqdm
import json

# Set up grid
chi_eff_grid = np.arange(-1,1,0.001)
chi_p_grid = np.arange(0,1,0.001)
CHI_EFF,CHI_P = np.meshgrid(chi_eff_grid,chi_p_grid)
dchi_eff = chi_eff_grid[1]-chi_eff_grid[0]
dchi_p = chi_p_grid[1]-chi_p_grid[0]

def calculate_Gaussian(mu_eff, sigma2_eff, mu_p, sigma2_p, cov):

    # Calculate and return bivariate probability density across grid
    grid = np.exp(-0.5/(1.-cov**2.)*(
                    np.square(CHI_EFF-mu_eff)/sigma2_eff
                    + np.square(CHI_P-mu_p)/sigma2_p
                    - 2.*cov*(CHI_EFF-mu_eff)*(CHI_P-mu_p)/np.sqrt(sigma2_eff*sigma2_p)
                    ))
    
    return grid

# Load data
datafile = "post_processed_current/posteriors_gaussian_spin_samples_FAR_1_in_1_w190814.json"
with open(datafile,'r') as jf:
    samps = json.load(jf)

mu_eff = np.array(samps['chiEff_mean'])
sig_eff = np.array(samps['chiEff_std'])
mu_p = np.array(samps['chiP_mean'])
sig_p = np.array(samps['chiP_std'])
corr = np.array(samps['rho_chiEff_chiP'])

# Randomly select subset of samples
nDraws = 500
randomInds = np.random.choice(np.arange(mu_eff.size),size=nDraws,replace=False)

# Instantiate arrays to hold pdfs, loop across samples
chi_eff_pdfs = np.zeros((randomInds.size,chi_eff_grid.size)) # (Number of pdf realizations, number of grid points)
chi_p_pdfs = np.zeros((randomInds.size,chi_p_grid.size))
for j,i in tqdm(enumerate(randomInds),total=nDraws):
    
    pdf_2d = calculate_Gaussian(mu_eff[i],sig_eff[i]**2.,mu_p[i],sig_p[i]**2.,corr[i]).T
    norm = np.sum(pdf_2d)*dchi_eff*dchi_p
        
    chi_eff_pdf = np.sum(pdf_2d/norm,axis=1)*dchi_p
    chi_p_pdf = np.sum(pdf_2d/norm,axis=0)*dchi_eff

    chi_eff_pdfs[j,:] = chi_eff_pdf
    chi_p_pdfs[j,:] = chi_p_pdf   

ppdDict = {}
ppdDict['chi_eff_grid'] = chi_eff_grid.tolist()
ppdDict['chi_p_grid'] = chi_p_grid.tolist()
ppdDict['chi_eff_pdfs'] = chi_eff_pdfs.tolist()
ppdDict['chi_p_pdfs'] = chi_p_pdfs.tolist()
ppdDict['pop_samples'] = {
    'mu_eff':mu_eff[randomInds].tolist(),
    'sig_eff':sig_eff[randomInds].tolist(),
    'mu_p':mu_p[randomInds].tolist(),
    'sig_p':sig_p[randomInds].tolist(),
    'rho':corr[randomInds].tolist()
}

with open('post_processed_current/gaussian-spin-w190814-xeff-xp-ppd-data.json','w') as jf:
    json.dump(ppdDict,jf)
