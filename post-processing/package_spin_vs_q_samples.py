import numpy as np
import json

samples = np.load("../results/results_11-29/processed_spin_vs_q_samples_FAR_1_in_1_r00.npy",allow_pickle=True)
betas = samples[:,10]
print(samples.shape)
samples = samples[betas<1.1,:]
print(samples.shape)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_logstd':samples[:,8].tolist(),
        'chi_alpha':samples[:,9].tolist(),
        'chi_beta':samples[:,10].tolist()
        }
with open("./post_processed_current/posteriors_samples_spin_vs_q_FAR_1_in_1.json","w") as jf:
    json.dump(dataDict,jf)

samples = np.load("../results/results_11-29/processed_spin_vs_q_samples_FAR_1_in_5_r00.npy",allow_pickle=True)
betas = samples[:,10]
samples = samples[betas<1.1,:]
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_logstd':samples[:,8].tolist(),
        'chi_alpha':samples[:,9].tolist(),
        'chi_beta':samples[:,10].tolist()
        }
with open("./post_processed_current/posteriors_samples_spin_vs_q_FAR_1_in_5.json","w") as jf:
    json.dump(dataDict,jf)

samples = np.load("../results/results_11-29/processed_spin_vs_q_samples_FAR_1_in_1_w190814_r00.npy",allow_pickle=True)
betas = samples[:,10]
samples = samples[betas<1.1,:]
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_logstd':samples[:,8].tolist(),
        'chi_alpha':samples[:,9].tolist(),
        'chi_beta':samples[:,10].tolist()
        }
with open("./post_processed_current/posteriors_samples_spin_vs_q_FAR_1_in_1_w190814.json","w") as jf:
    json.dump(dataDict,jf)
