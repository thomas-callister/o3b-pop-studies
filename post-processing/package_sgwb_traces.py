import numpy as np
import json

bbh_samples = np.load("../stochastic-update/results_11-29/omegaGW_savedBBH.npy",allow_pickle=True)[()]
dataDict = {
        'OmgGW':bbh_samples['OmgGW'].tolist(),
        'freqs':bbh_samples['f'].tolist()
        }
with open("./post_processed_current/omegaGW_bbh.json","w") as jf:
    json.dump(dataDict,jf)

bns_samples = np.load("../stochastic-update/results_11-29/omegaGW_savedBNS.npy",allow_pickle=True)[()]
dataDict = {
        'OmgGW':bns_samples['OmgGW'].tolist(),
        'freqs':bns_samples['f'].tolist()
        }
with open("./post_processed_current/omegaGW_bns.json","w") as jf:
    json.dump(dataDict,jf)

nsbh_samples = np.load("../stochastic-update/results_11-29/omegaGW_savedNSBH.npy",allow_pickle=True)[()]
dataDict = {
        'OmgGW':nsbh_samples['OmgGW'].tolist(),
        'freqs':nsbh_samples['f'].tolist()
        }
with open("./post_processed_current/omegaGW_nsbh.json","w") as jf:
    json.dump(dataDict,jf)

