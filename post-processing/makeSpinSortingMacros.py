import json
import numpy as np

spinSortingFile = "/home/sylvia.biscoveanu/O3/pop_data/spin_sorting/gwpop_June21/o3only_mass_c_iid_mag_iid_tilt_powerlaw_redshift_result_chiA_chiB.json"
with open(spinSortingFile,"r") as jf:   
    spinData = json.load(jf)

paramData = spinData['metadata']
for key in paramData:
    for k,v in paramData[key].items():
        paramData[key][k] = round(v,2)

with open('post_processed_current/SpinSortingMacros.json','w') as jf:
    json.dump(paramData,jf,sort_keys=True,indent=4)
