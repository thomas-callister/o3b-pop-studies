#!/bin/bash

python package_gaussian_spin_samples.py
python package_spin_vs_q_samples.py
python package_truncated_spin_samples.py

python makeSpinSortingMacros.py
python makeSpinTruncationMacros.py

python make_gaussian_spin_ppd_data.py
python make_default_effective_data.py
