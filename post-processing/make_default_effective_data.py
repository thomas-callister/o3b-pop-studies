import numpy as np
from scipy.stats import gaussian_kde
from scipy.special import betaincinv
from tqdm import tqdm
import json

def Calculate_Xp(m1, m2, a1, a2, costilt1, costilt2):

    # Mass ratio 
    q_inv = 1.0*m1/m2

    # Coefficients A1 and A2 
    A1 = 2 + 3/(2*q_inv)
    A2 = 2 + 3*q_inv/2

    # Calculating sin from cos
    sintilt1 = np.sqrt(1-(costilt1)**2)
    sintilt2 = np.sqrt(1-(costilt2)**2)

    # Terms in Xp
    term1 = A1*a1*(m1**2)*sintilt1
    term2 = A2*a2*(m2**2)*sintilt2
    coeff = 1.0/(2*A1*(m1**2))

    # Using addition of terms method 
    Xp = coeff*(term1 + term2 + np.abs(term1 - term2))

    return Xp

# Load default spin results
defaultData = "/home/thomas.callister/CBC/o3b-population-data/analyses/PowerLawPeak/o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_result.json"

with open(defaultData,"r") as jfile:

    jf = json.load(jfile)  
    posterior_samples = jf['posterior']['content']

    # Read out parameters
    mass_alphas = np.array(posterior_samples['alpha'])
    mass_betas = np.array(posterior_samples['beta'])
    delta_ms = np.array(posterior_samples['delta_m'])
    mmins = np.array(posterior_samples['mmin'])
    mmaxs = np.array(posterior_samples['mmax'])
    lams = np.array(posterior_samples['lam'])
    mu_ms = np.array(posterior_samples['mpp'])
    sig_ms = np.array(posterior_samples['sigpp'])
    mu_x = np.array(posterior_samples['mu_chi'])
    sigma2_x = np.array(posterior_samples['sigma_chi'])
    zetas = np.array(posterior_samples['xi_spin'])
    sig_ts = np.array(posterior_samples['sigma_spin'])

# Convert spin mean/variances to beta distribution alpha and beta parameters
spin_alphas = ((1.-mu_x)/sigma2_x - 1./mu_x)*mu_x*mu_x
spin_betas = spin_alphas*(1./mu_x-1.)

# Simulate 1000 populations, each containing 1000 events
m1s = np.zeros((1000,1000)) # (number of pops, number of events per pop)
m2s = np.zeros((1000,1000))
a1s = np.zeros((1000,1000))
a2s = np.zeros((1000,1000))
cost1s = np.zeros((1000,1000))
cost2s = np.zeros((1000,1000))

# Choose 1000 random draws from the hyperposterior
inds = np.random.choice(np.arange(0,lams.size),size=1000,replace=False)

# Loop across draws
for i,ind in tqdm(enumerate(inds),total=1000):

    # Read off the hyperposterior sample
    lam = lams[ind]
    mmin = mmins[ind]
    mmax = mmaxs[ind]
    alpha_m = mass_alphas[ind]
    beta_m = mass_betas[ind]
    sig_m = sig_ms[ind]
    mu_m = mu_ms[ind]
    delta_m = delta_ms[ind]
    a = spin_alphas[ind]
    b = spin_betas[ind]
    
    #######
    # First, we need to build the probability distribution on m1
    # We'll separately compute the power-law and gaussian pices and combine,
    # then apply low-mass smoothing, then re-normalize
    #######

    # Define a reference grid over primary masses
    ref_ms = np.linspace(mmin,mmax,400)
    
    # Power law m1 component, normalized on (mmin,mmax)
    p_m1_powerLaw = (1.-alpha_m)*np.power(ref_ms,-alpha_m)/(np.power(mmax,1.-alpha_m)-np.power(mmin,1.-alpha_m))
    
    # Gaussian m1 component, normalized over -inf to inf
    p_m1_gaussian = (1./np.sqrt(2.*np.pi*sig_m**2.))*np.exp(-0.5*(ref_ms-mu_m)*(ref_ms-mu_m)/sig_m**2.)

    # Apply smooth turn-on at masses below mmin + delta_m
    smoothingFactors = np.ones(ref_ms.size)
    masses_to_smooth = ref_ms[(ref_ms<mmin+delta_m)]
    smoothingFactors[(ref_ms<mmin+delta_m)] = 1./(np.exp(delta_m/(masses_to_smooth-mmin) - delta_m/(delta_m-(masses_to_smooth-mmin)))+1.)

    # Incorporate smoothing and re-normalize
    p_m1 = ((1.-lam)*p_m1_powerLaw + lam*p_m1_gaussian)*smoothingFactors
    p_m1 /= np.trapz(p_m1,ref_ms)

    # Build interpolant for CDF(m1), which we use to draw a random set of primary masses
    cdf_vals = np.cumsum(p_m1)*(ref_ms[1]-ref_ms[0])   
    new_m1s = np.interp(np.random.random(1000),cdf_vals,ref_ms)
    m1s[i,:] = new_m1s

    # Draw secondary masses
    new_m2s = np.power(np.random.random(1000)*(new_m1s**(1.+beta_m) - mmin**(1.+beta_m)) + mmin**(1.+beta_m),1./(1+beta_m))
    m2s[i,:] = new_m2s
    
    # Draw component spin magnitudes
    a1s[i,:] = betaincinv(a,b,np.random.random(1000))
    a2s[i,:] = betaincinv(a,b,np.random.random(1000))
    
    # Spin tilts
    # Recall that zeta is the fraction of events within the Gaussian sub-population
    for j in range(1000):
    
        # Draw a random number to determine if we're in the "isotropic" or "aligned" sub-population
        branch = np.random.random()
        if branch<zetas[ind]:
        
            # Draw from a gaussian in cos(t)
            cost1 = -10.        
            while (cost1<-1) or (cost1>1):
                cost1 = np.random.normal(loc=1.,scale=sig_ts[ind])

            cost2 = -10.
            while (cost2<-1) or (cost2>1):
                cost2 = np.random.normal(loc=1.,scale=sig_ts[ind]) 

        # Otherwise draw random spins
        else:
            cost1 = 2.*np.random.random()-1.
            cost2 = 2.*np.random.random()-1.
            
        cost1s[i,j] = cost1
        cost2s[i,j] = cost2 

# Compute chi-effective and chi-p values
chiEffs_default = (m1s*a1s*cost1s+m2s*a2s*cost2s)/(m1s+m2s)
chiPs_default = Calculate_Xp(m1s,m2s,a1s,a2s,cost1s,cost2s)

## KDE smooth the resulting chiEffective and chiP distributions

# Fixed grids on which to evaluate KDEs
chi_eff_grid = np.linspace(-1,1,200)
chi_p_grid = np.linspace(0,1,200)

# Placeholder arrays to hold KDE'd probability distributions
chi_eff_pdfs = np.zeros((200,1000)) # (number of chi gridpoints, number of populations)
chi_p_pdfs = np.zeros((200,1000))

# Loop across our distributions and KDE
for i in tqdm(range(1000)):
    
    # Chi-Effective KDE
    kde_chiEff = gaussian_kde(chiEffs_default[i,:])
    
    # Evaluate on our grid, normalize, and save
    single_chiEff_pdf = kde_chiEff(chi_eff_grid)
    kde_norm = np.trapz(single_chiEff_pdf,chi_eff_grid)   
    chi_eff_pdfs[:,i] = single_chiEff_pdf/kde_norm
    
    # Chi-P KDE
    kde_chiP = gaussian_kde(chiPs_default[i,:])
    
    # Evaluate, normalize, save
    single_chiP_pdf = kde_chiP(chi_p_grid)
    kde_norm = np.trapz(single_chiP_pdf,chi_p_grid)   
    chi_p_pdfs[:,i] = single_chiP_pdf/kde_norm

# Let's save our data
dataDict = {}

# Store the random draws from the population parameters
dataDict['pop_samples'] = {
    'lam':lams[inds].tolist(),
    'mmin':mmins[inds].tolist(),
    'mmax':mmaxs[inds].tolist(),
    'alpha':mass_alphas[inds].tolist(),
    'beta':mass_betas[inds].tolist(),
    'sigpp':sig_ms[inds].tolist(),
    'mpp':mu_ms[inds].tolist(),
    'delta_m':delta_ms[inds].tolist(),
    'mu_chi':mu_x[inds].tolist(),
    'sigma_chi':sigma2_x[inds].tolist(),
    'xi_spin':zetas[inds].tolist(),
    'sigma_spin':sig_ts[inds].tolist()
    }

# Store PDFs
dataDict['chi_eff_grid'] = chi_eff_grid.tolist()
dataDict['chi_eff_pdfs'] = chi_eff_pdfs.tolist()
dataDict['chi_p_grid'] = chi_p_grid.tolist()
dataDict['chi_p_pdfs'] = chi_p_pdfs.tolist()

with open('./post_processed_current/default-spin-xeff-xp-ppd-data.json','w') as jf:
    json.dump(dataDict,jf)
