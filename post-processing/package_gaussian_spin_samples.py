import numpy as np
import json

samples = np.load("../results/results_11-29/processed_gaussian_spin_samples_FAR_1_in_1_r00.npy",allow_pickle=True)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_std':samples[:,8].tolist(),
        'chiP_mean':samples[:,9].tolist(),
        'chiP_std':samples[:,10].tolist(),
        'rho_chiEff_chiP':samples[:,11].tolist()
        }
with open("./post_processed_current/posteriors_gaussian_spin_samples_FAR_1_in_1.json","w") as jf:
    json.dump(dataDict,jf)

samples = np.load("../results/results_11-29/processed_gaussian_spin_samples_FAR_1_in_5_r00.npy",allow_pickle=True)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_std':samples[:,8].tolist(),
        'chiP_mean':samples[:,9].tolist(),
        'chiP_std':samples[:,10].tolist(),
        'rho_chiEff_chiP':samples[:,11].tolist()
        }
with open("./post_processed_current/posteriors_gaussian_spin_samples_FAR_1_in_5.json","w") as jf:
    json.dump(dataDict,jf)

samples = np.load("../results/results_11-29/processed_gaussian_spin_samples_FAR_1_in_1_w190814_r00.npy",allow_pickle=True)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_std':samples[:,8].tolist(),
        'chiP_mean':samples[:,9].tolist(),
        'chiP_std':samples[:,10].tolist(),
        'rho_chiEff_chiP':samples[:,11].tolist()
        }
with open("./post_processed_current/posteriors_gaussian_spin_samples_FAR_1_in_1_w190814.json","w") as jf:
    json.dump(dataDict,jf)
