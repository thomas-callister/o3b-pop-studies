import numpy as np
import json

truncationDict = {}

o3a_file = "/home/thomas.callister/CBC/o3-pop/Population_Samples/gaussian-spin/gaussian-chiEff-variable-chiMin-samples.json"
with open(o3a_file,'r') as jf:
    o3a_data = json.load(jf)

trunc_bound_O3a = np.array(o3a_data['chi_eff_min'])
truncationDict['O3a'] = {\
        'chi_min_percentile_above_zero':100.*round(float(trunc_bound_O3a[trunc_bound_O3a<0.].size)/trunc_bound_O3a.size,3)\
        }

o3b_file = "/home/thomas.callister/CBC/o3b-pop-studies/post-processing/post_processed_current/posteriors_truncatedGaussian_chiEffOnly_spin_samples_FAR_1_in_1.json"
with open(o3b_file,'r') as jf:
    o3b_data = json.load(jf)
trunc_bound_O3b = np.array(o3b_data['chiEff_min'])

o3b_mixtureBulk_file = "/home/thomas.callister/CBC/o3b-pop-studies/post-processing/post_processed_current/posteriors_truncatedMixtureModel_samples_FAR_1_in_1_bulkTruncated.json"
with open(o3b_mixtureBulk_file,'r') as jf:
    o3b_mixtureBulk_data = json.load(jf)
trunc_bound_O3b_mixtureBulk = np.array(o3b_mixtureBulk_data['chiEff_min'])
zeta_bulk_O3b_mixtureBulk = np.array(o3b_mixtureBulk_data['zeta_bulk'])

truncationDict['O3b'] = {\
        'chi_min_percentile_above_zero':100.*round(float(trunc_bound_O3b[trunc_bound_O3b<0.].size)/trunc_bound_O3b.size,3),
        'chi_min_percentile_above_zero_mixtureBulk':100.*round(float(trunc_bound_O3b_mixtureBulk[trunc_bound_O3b_mixtureBulk<0.].size)/trunc_bound_O3b_mixtureBulk.size,3),
        'zeta_bulk_99p_lowerBound':round(np.quantile(zeta_bulk_O3b_mixtureBulk,0.01),2)\
        }

with open('post_processed_current/SpinTruncationMacros.json','w') as jf:
    json.dump(truncationDict,jf,sort_keys=True,indent=4)

print(trunc_bound_O3b[trunc_bound_O3b<0.].size,trunc_bound_O3b.size)
print(trunc_bound_O3b_mixtureBulk[trunc_bound_O3b_mixtureBulk<0.].size,trunc_bound_O3b_mixtureBulk.size)
