import numpy as np
import json

samples = np.load("../results/results_11-29/processed_truncatedGaussian_chiEffOnly_spin_samples_FAR_1_in_1_r03r04.npy",allow_pickle=True)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_std':samples[:,8].tolist(),
        'chiEff_min':samples[:,9].tolist(),
        }
with open("./post_processed_current/posteriors_truncatedGaussian_chiEffOnly_spin_samples_FAR_1_in_1.json","w") as jf:
    json.dump(dataDict,jf)

samples = np.load("../results/results_11-29/processed_truncatedMixtureModel_samples_FAR_1_in_1_bulkTruncated_r01.npy",allow_pickle=True)
dataDict = {
        'lmbda':samples[:,0].tolist(),
        'mMax':samples[:,1].tolist(),
        'm0':samples[:,2].tolist(),
        'sigM':samples[:,3].tolist(),
        'peak_fraction':samples[:,4].tolist(),
        'beta_q':samples[:,5].tolist(),
        'kappa':samples[:,6].tolist(),
        'chiEff_mean':samples[:,7].tolist(),
        'chiEff_std':samples[:,8].tolist(),
        'zeta_bulk':samples[:,9].tolist(),
        'chiEff_min':samples[:,10].tolist(),
        }
with open("./post_processed_current/posteriors_truncatedMixtureModel_samples_FAR_1_in_1_bulkTruncated.json","w") as jf:
    json.dump(dataDict,jf)

