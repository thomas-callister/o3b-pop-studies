#!/bin/bash

#cp post_processed_current/posteriors_gaussian_spin_samples* /home/thomas.callister/CBC/o3b-population-data/analyses/GaussianSpin/
#cp post_processed_current/posteriors_truncated* /home/thomas.callister/CBC/o3b-population-data/analyses/GaussianSpin/
#cp post_processed_current/posteriors_samples_spin_vs_q* /home/thomas.callister/CBC/o3b-population-data/analyses/SpinMassRatioCorrelations/

#cp post_processed_current/gaussian-spin-xeff-xp-ppd-data.json /home/thomas.callister/CBC/o3b-population-data/analyses/GaussianSpin

#cp post_processed_current/omegaGW_*.json /home/thomas.callister/CBC/o3b-population-data/analyses/Stochastic/

cp posterior_sample_dict.pickle.bz2 /home/thomas.callister/CBC/o3b-population-data/postproc/

