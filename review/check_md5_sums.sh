#!/bin/bash

# Loop across every file that has been copied to the O3b data repository
echo -e "\nGAUSSIAN SPIN:"
for file in /home/thomas.callister/CBC/o3b-population-data/analyses/GaussianSpin/*; do

    echo

    # MD5sum of file in data repo
    md5sum $file

    # Find the file of the same name in our local analysis repo, and check its md5sum
    # Note that this will (purposefully) fail if there *isn't* any such local file
    md5sum ../post-processing/post_processed_current/`basename $file`

done

# Loop across every file that has been copied to the O3b data repository
echo -e "\nSPIN VS Q:"
for file in /home/thomas.callister/CBC/o3b-population-data/analyses/SpinMassRatioCorrelations/*; do

    echo

    # MD5sum of file in data repo
    md5sum $file

    # Find the file of the same name in our local analysis repo, and check its md5sum
    # Note that this will (purposefully) fail if there *isn't* any such local file
    md5sum ../post-processing/post_processed_current/`basename $file`

done
