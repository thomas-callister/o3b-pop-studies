import numpy as np
import emcee as mc
import h5py
from scipy.stats import gaussian_kde
from scipy.special import erf

# -- Set prior bounds --
mMin_min = 1.
mMin_max = 10.
mMax_min = 65.
mMax_max = 100.
alpha_min = -6.
alpha_max = 0.
bq_min = 0.
bq_max = 8.
kappa_min = -4.
kappa_max = 12.
d_mMax_min = -10.
d_mMax_max = 30.
d_alpha_min = -5.
d_alpha_max = 5.

# Dicts with samples: 
sampleDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/input/sampleDict_FAR_1_in_1_yr_11-29.pickle")
sampleDict.pop('S190814bv')

# Load mock detections
injectionDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/code/injectionDict_10-20_directMixture_FAR_1_in_1.pickle")

# -- Log posterior function -- 
def logposterior(c):

    # Read parameters
    mMin = c[0]
    mMax = c[1]
    alpha = c[2]
    bq = c[3]
    kappa = c[4]
    dm = c[5]
    da = c[6]

    # Flat priors, reject samples past boundaries
    if mMin<mMin_min or mMin>mMin_max or mMax<mMax_min or mMax>mMax_max or alpha<alpha_min or alpha>alpha_max or bq<bq_min or bq>bq_max or dm<d_mMax_min or dm>d_mMax_max or da<d_alpha_min or da>d_alpha_max or mMax+2.*dm<mMin:
        return -np.inf
    
    # If sample in prior range, evaluate
    else:
        logP = 0.

        # Unpack injections
        m1_det = injectionDict['m1']
        m2_det = injectionDict['m2']
        z_det = injectionDict['z']
        dVdz_det = injectionDict['dVdz']
        Xeff_det = injectionDict['Xeff']
        pop_reweight = injectionDict['weights_XeffOnly']

        nEvents = len(sampleDict)

        alphas_inj = alpha + da*z_det
        mMaxs_inj = mMax + dm*z_det
        p_m1_inj = (1.+alphas_inj)*m1_det**alphas_inj/(mMaxs_inj**(1.+alphas_inj)-mMin**(1.+alphas_inj))
        p_m2_inj = (1.+bq)*m2_det**bq/(m1_det**(1.+bq)-mMin**(1.+bq))
        p_m2_inj[np.isinf(p_m2_inj)] = 0.

        p_xeff_inj = np.exp(-(Xeff_det-0.05)**2/2.*0.1**2)

        p_z_inj = dVdz_det*(1.+z_det)**(kappa-1.)
        p_m1_inj[m1_det>mMaxs_inj] = 0.
        p_m2_inj[m2_det<mMin] = 0.

        det_weights = p_m1_inj*p_m2_inj*p_xeff_inj*p_z_inj*pop_reweight
        if np.max(det_weights)==0:
            return -np.inf
        Nsamp = np.sum(det_weights)**2/np.sum(det_weights**2)
        if Nsamp<=4*nEvents:
            print("Insufficient mock detections:",c)
            return -np.inf
        log_detEff = -nEvents*np.log(np.sum(det_weights))
        logP += log_detEff
                
        for event in sampleDict:

            # Grab samples
            z_sample = sampleDict[event]['z']
            m1_sample = sampleDict[event]['m1']
            m2_sample = sampleDict[event]['m2']
            xeff_sample = sampleDict[event]['Xeff']
            weights = sampleDict[event]['weights']
            priors = sampleDict[event]['Xeff_priors']
            
            alphas = alpha + da*z_sample
            mMaxs = mMax + dm*z_sample
            p_m1 = (1.+alphas)*m1_sample**alphas/(mMaxs**(1.+alphas)-mMin**(1.+alphas))
            p_m2 = (1.+bq)*m2_sample**bq/(m1_sample**(1.+bq)-mMin**(1.+bq))
            p_z = (1.+z_sample)**kappa
            p_m1[m1_sample>mMaxs] = 0.
            p_m2[m2_sample<mMin] = 0.

            p_xeff = np.exp(-(xeff_sample-0.05)**2/(2.*0.1**2))
            
            # Evaluate marginalized likelihood
            nSamples = p_m1.size
            pEvidence = np.sum(p_m1*p_m2*p_z*p_xeff*weights/(1.+z_sample)**2.7/priors)/nSamples

            # Summation
            logP += np.log(pEvidence)

        print(logP)
            
        return logP
    
    

    
# -- Running mcmc --     
if __name__=="__main__":

    # Initialize walkers from random positions in mu-sigma2 parameter space
    nWalkers = 16
    initial_mMins = np.random.random(nWalkers)+3.
    initial_mMaxs = np.random.random(nWalkers)*20+50.
    initial_alphas = np.random.random(nWalkers)*(-2.)-4.
    initial_bqs = np.random.random(nWalkers)+1.
    initial_kappas = np.random.random(nWalkers)*2.
    initial_dms = 0.1*np.random.random(nWalkers)-0.05
    initial_das = 0.1*np.random.random(nWalkers)-0.05
    initial_walkers = np.transpose([initial_mMins,initial_mMaxs,initial_alphas,initial_bqs,initial_kappas,initial_dms,initial_das])
    
    print('Initial walkers:')
    print(initial_walkers)
    
    # Dimension of parameter space
    dim = 7

    # Run
    output = "/home/thomas.callister/CBC/o3b-pop-studies/mass-vs-redshift-update/results_11-29/emcee_samples_truncated_evolving_FAR_1_in_1"
    nSteps = 20000
    sampler = mc.EnsembleSampler(nWalkers,dim,logposterior,threads=16)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        if i%50==0:
            np.save(output,sampler.chain)
    np.save(output,sampler.chain)
