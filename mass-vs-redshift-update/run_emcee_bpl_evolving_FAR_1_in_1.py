import numpy as np
import emcee as mc
import h5py
from scipy.stats import gaussian_kde
from scipy.special import erf
import sys

# -- Set prior bounds --
mMin_min = 1.
mMin_max = 10.
alpha1_min = -5
alpha1_max = 4
alpha2_min = -12
alpha2_max = -1
m0_min = 20
m0_max = 100
bq_min = -2.
bq_max = 10.
kappa_min = -4.
kappa_max = 12.
d_m0_min = -8.
d_m0_max = 30.
d_alpha_min = -3.
d_alpha_max = 3.

# Dicts with samples: 
sampleDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/input/sampleDict_FAR_1_in_1_yr_11-29.pickle")
sampleDict.pop('S190814bv')

# Load mock detections
injectionDict = np.load("/home/thomas.callister/CBC/o3b-pop-studies/code/injectionDict_10-20_directMixture_FAR_1_in_1.pickle")

# -- Log posterior function -- 
def logposterior(c):

    # Read parameters
    mMin = c[0]
    alpha1 = c[1]
    alpha2 = c[2]
    m0 = c[3]
    bq = c[4]
    kappa = c[5]
    dm = c[6]
    da = c[7]

    # Flat priors, reject samples past boundaries
    if mMin<mMin_min or mMin>mMin_max or alpha1<alpha1_min or alpha1>alpha1_max or alpha2<alpha2_min or alpha2>alpha2_max or m0<m0_min or m0>m0_max or bq<bq_min or bq>bq_max or kappa<kappa_min or kappa>kappa_max or dm<d_m0_min or dm>d_m0_max or da<d_alpha_min or da>d_alpha_max or alpha2+1.5*da>-1. or mMin+2.*dm<0.:
        return -np.inf
    
    # If sample in prior range, evaluate
    else:
        logP = 0.

        # Unpack injections
        m1_det = injectionDict['m1']
        m2_det = injectionDict['m2']
        z_det = injectionDict['z']
        dVdz_det = injectionDict['dVdz']
        Xeff_det = injectionDict['Xeff']
        pop_reweight = injectionDict['weights_XeffOnly']

        nEvents = len(sampleDict)
        alpha2s_inj = alpha2 + da*z_det
        m0s_inj = m0 + dm*z_det

        # Reweight found injections
        p_m1_norm = (1.+alpha1)*(1.+alpha2s_inj)/(m0s_inj*(alpha2s_inj-alpha1)-mMin*np.power(mMin/m0s_inj,alpha1)*(1.+alpha2s_inj))
        p_m1_inj = np.ones(m1_det.size)
        low_m1_dets = m1_det<m0s_inj
        high_m1_dets = m1_det>=m0s_inj
        p_m1_inj[low_m1_dets] = p_m1_norm[low_m1_dets]*np.power(m1_det[low_m1_dets]/m0s_inj[low_m1_dets],alpha1)
        p_m1_inj[high_m1_dets] = p_m1_norm[high_m1_dets]*np.power(m1_det[high_m1_dets]/m0s_inj[high_m1_dets],alpha2s_inj[high_m1_dets])

        p_m2_inj = (1.+bq)*m2_det**bq/(m1_det**(1.+bq)-mMin**(1.+bq))
        p_z_inj = dVdz_det*(1.+z_det)**(kappa-1.)
        p_m2_inj[m2_det<mMin] = 0.

        p_xeff_inj = np.exp(-(Xeff_det-0.05)**2/2.*0.1**2)

        det_weights = p_m1_inj*p_m2_inj*p_z_inj*p_xeff_inj*pop_reweight
        if np.max(det_weights)==0:
            return -np.inf
        Nsamp = np.sum(det_weights)**2/np.sum(det_weights**2)
        if Nsamp<=4*nEvents:
            print("Insufficient mock detections:",c,Nsamp)
            return -np.inf
        log_detEff = -nEvents*np.log(np.sum(det_weights))
        logP += log_detEff

        for event in sampleDict:

            # Grab samples
            z_sample = sampleDict[event]['z']
            m1_sample = sampleDict[event]['m1']
            m2_sample = sampleDict[event]['m2']
            xeff_sample = sampleDict[event]['Xeff']
            weights = sampleDict[event]['weights']
            priors = sampleDict[event]['Xeff_priors']
            
            alpha2s = alpha2 + da*z_sample
            m0s = m0 + dm*z_sample

            p_m1_norm = (1.+alpha1)*(1.+alpha2s)/(m0s*(alpha2s-alpha1)-mMin*np.power(mMin/m0s,alpha1)*(1.+alpha2s))

            p_m1 = np.ones(m1_sample.size)
            low_ms = m1_sample<m0s
            high_ms = m1_sample>=m0s
            p_m1[low_ms] = p_m1_norm[low_ms]*np.power(m1_sample[low_ms]/m0s[low_ms],alpha1)
            p_m1[high_ms] = p_m1_norm[high_ms]*np.power(m1_sample[high_ms]/m0s[high_ms],alpha2s[high_ms])

            p_m2 = (1.+bq)*m2_sample**bq/(m1_sample**(1.+bq)-mMin**(1.+bq))
            p_z = (1.+z_sample)**kappa
            p_m2[m2_sample<mMin] = 0.

            p_xeff = np.exp(-(xeff_sample-0.05)**2/(2.*0.1**2))
            
            # Evaluate marginalized likelihood
            nSamples = p_m1.size
            pEvidence = np.sum(p_m1*p_m2*p_z*p_xeff*weights/(1.+z_sample)**2.7/priors)/nSamples

            # Summation
            logP += np.log(pEvidence)
            
        print(logP)
        return logP
    
# -- Running mcmc --     
if __name__=="__main__":

    # Initialize walkers from random positions in mu-sigma2 parameter space
    nWalkers = 16
    initial_mMins = np.random.random(nWalkers)+3.
    initial_alpha1s = np.random.random(nWalkers)*(-2.)-4.
    initial_alpha2s = np.random.random(nWalkers)*(-2.)-5.
    initial_m0s = np.random.random(nWalkers)*40.+20
    initial_bqs = np.random.random(nWalkers)+1.
    initial_kappas = np.random.random(nWalkers)*2.
    initial_dms = np.random.random(nWalkers)*(d_m0_max-d_m0_min)+d_m0_min
    initial_das = np.random.random(nWalkers)*(d_alpha_max-d_alpha_min)+d_alpha_min
    initial_walkers = np.transpose([initial_mMins,initial_alpha1s,initial_alpha2s,initial_m0s,initial_bqs,initial_kappas,initial_dms,initial_das])
    
    print('Initial walkers:')
    print(initial_walkers)
    
    # Dimension of parameter space
    dim = 8

    # Run
    output = "/home/thomas.callister/CBC/o3b-pop-studies/mass-vs-redshift-update/results_11-29/emcee_samples_bpl_evolving_FAR_1_in_1"
    nSteps = 20000
    sampler = mc.EnsembleSampler(nWalkers,dim,logposterior,threads=16)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        if i%10==0:
            np.save(output,sampler.chain)
    np.save(output,sampler.chain)
